{% docs sfdc_accounts_xf_col_ultimate_parent_account_id %}

Salesforce Account ID for Ultimate Parent Account

{% enddocs %}

{% docs sfdc_accounts_xf_col_ultimate_parent_account_name %}

Salesforce Account Name for Ultimate Parent Account

{% enddocs %}