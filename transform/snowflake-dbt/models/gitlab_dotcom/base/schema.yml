version: 2

models:
  - name: gitlab_dotcom_approvals
    description: Base model for Gitlab.com approvals
    columns:
      - name: approval_id
        tests:
          - not_null
          - unique
      - name: approval_created_at
        tests:
          - not_null
      - name: approval_updated_at
        tests:
          - not_null


  - name: gitlab_dotcom_merge_request_metrics
    description: Base model for Gitlab.com merge request metrics
    columns:
      - name: merge_request_metric_id
        tests:
          - not_null
          - unique
      - name: merge_request_id
        tests:
          - not_null
          - relationships:
              to: ref('gitlab_dotcom_merge_requests')
              field: merge_request_id
      - name: merge_request_metric_created_at
        tests:
          - not_null
      - name: merge_request_metric_updated_at
        tests:
          - not_null
      - name: merge_request_metrics_last_updated_at
        description: Last run of the script that loaded this table

  - name: gitlab_dotcom_approver_groups
    description: Base model for Gitlab.com approver groups
    columns:
      - name: approver_group_id
        tests:
          - not_null
          - unique
      - name: approver_group_created_at
        tests:
          - not_null
      - name: approver_group_updated_at
        tests:
          - not_null

  - name: gitlab_dotcom_merge_requests
    description: Base model for Gitlab.com merge requests, column `merge_params` is hidden for privacy concerns.
    columns:
      - name: merge_request_id
        tests:
          - not_null
          - unique
      - name: merge_request_created_at
        tests:
          - not_null
      - name: merge_request_updated_at
        tests:
          - not_null
      - name: is_merge_to_master
        description: This fields queries if the target branch name is "master" and returns a boolean
      - name: merge_requests_last_updated_at
        description: Last run of the script that imported this table.

  - name: gitlab_dotcom_board_assignees
    description: Base model for Gitlab.com Board Assignees
    columns:
      - name: board_assignee_relation_id
        tests:
          - not_null
          - unique
      - name: board_id
        tests:
          - not_null
          - relationships:
              to: ref('gitlab_dotcom_boards')
              field: board_id

  - name: gitlab_dotcom_merge_requests_closing_issues
    description: Base model for Gitlab.com merge requests closing issues
    columns:
      - name: merge_request_issue_relation_id
        description: unique id created with an md5 hash of `issue_id`, `merge_request_id`, and `created_at`
        tests:
          - not_null
          - unique
      - name: merge_request_id
        tests:
          - not_null
      - name: issue_id
        tests:
          - not_null
      - name: merge_request_closing_issue_created_at
        tests:
          - not_null
      - name: merge_request_closing_issue_created_at
        tests:
          - not_null

  - name: gitlab_dotcom_board_labels
    description: Base model for Gitlab.com board labels
    columns:
      - name: board_label_relation_id
        tests:
          - not_null
          - unique
      - name: board_id
        tests:
          - not_null
      - name: label_id
        tests:
          - not_null
      - name: labels_last_updated_at
        description: Last run of the script that loaded this table

  - name: gitlab_dotcom_milestones
    description: Base model for Gitlab.com milestones
    columns:
      - name: milestone_id
        tests:
          - not_null
          - unique
      - name: milestone_created_at
        tests:
          - not_null
      - name: milestone_updated_at
        tests:
          - not_null

  - name: gitlab_dotcom_boards
    description: Base model for Gitlab.com board
    columns:
      - name: board_id
        tests:
          - not_null
          - unique
      - name: board_created_at
        tests:
          - not_null
      - name: board_created_at
        tests:
          - not_null

  - name: gitlab_dotcom_namespace_statistics
    description: Base model for Gitlab.com namespace statistics
    columns:
      - name: namespace_statistics_id
        tests:
          - not_null
          - unique
      - name: namespace_id
        tests:
          - not_null
          - relationships:
              to: ref('gitlab_dotcom_namespaces')
              field: namespace_id

  - name: gitlab_dotcom_epic_issues
    description: Base model for Gitlab.com epic issues
    columns:
      - name: epic_issues_relation_id
        tests:
          - not_null
          - unique
      - name: epic_id
        tests:
          - not_null
          - relationships:
              to: ref('gitlab_dotcom_epics')
              field: epic_id
      - name: issue_id
        tests:
          - not_null
          - relationships:
              to: ref('gitlab_dotcom_issues')
              field: issue_id

  - name: gitlab_dotcom_namespaces
    description: Base model for Gitlab.com namespaces
    columns:
      - name: namespace_id
        tests:
          - not_null
          - unique
      - name: namespace_created_at
        tests:
          - not_null
      - name: namespace_updated_at
        tests:
          - not_null
      - name: namespace_path
        description: '{{ doc("visibility_documentation") }}'
      - name: namespace_name
        description: '{{ doc("visibility_documentation") }}'
      - name: visbility_level
        description: '{{ doc("visibility_documentation") }}'

  - name: gitlab_dotcom_epic_metrics
    description: Base model for Gitlab.com epic metrics
    columns:
      - name: epic_id
        tests:
          - not_null
          - unique
          - relationships:
              to: ref('gitlab_dotcom_epics')
              field: epic_id
      - name: epic_metrics_created_at
        tests:
          - not_null
      - name: epic_metrics_updated_at
        tests:
          - not_null
      - name: epic_metrics_last_updated
        description: last run of the script that imported this table

  - name: gitlab_dotcom_notification_settings
    description: Base model for Gitlab.com notification settings
    columns:
      - name: notification_settings_id
        tests:
          - not_null
          - unique
      - name: user_id
        tests:
          - not_null
      - name: notification_settings_created_at
        tests:
          - not_null
      - name: notification_settings_updated_at
        tests:
          - not_null

  - name: gitlab_dotcom_epics
    description: Base model for Gitlab.com epics
    columns:
      - name: epic_id
        tests:
          - not_null
          - unique
      - name: epic_created_at
        tests:
          - not_null
      - name: epic_created_at
        tests:
          - not_null
      - name: epics_last_updated
        description: last run of the script that imported this table

  - name: gitlab_dotcom_project_authorizations
    description: Base model for Gitlab.com authorizations, duplicate rows have not been included.
    columns:
      - name: user_project_access_relation_id
        description: unique key created with an md5 hash of user_id, project_id and access_level
        tests:
            - not_null
            - unique
      - name: user_id
        tests:
          - not_null
      - name: project_id
        tests:
          - not_null
      - name: access_level
        tests:
          - not_null

  - name: gitlab_dotcom_issue_assignees
    description: Base model for Gitlab.com issue assignees, duplicate rows have not been included.
    columns:
      - name: user_issue_relation_id
        description: unique key created with an md5 hash of user_id and issue_id
        tests:
            - not_null
            - unique
      - name: user_id
        tests:
          - not_null
          - relationships:
              to: ref('gitlab_dotcom_users')
              field: user_id
      - name: issue_id
        tests:
          - not_null
          - relationships:
              to: ref('gitlab_dotcom_issues')
              field: issue_id

  - name: gitlab_dotcom_project_auto_devops
    description: Base model for Gitlab.com auto devops
    columns:
      - name: project_id
        tests:
          - not_null
          - unique
      - name: project_auto_devops_created_at
        tests:
          - not_null
      - name: project_auto_devops_updated_at
        tests:
          - not_null

  - name: gitlab_dotcom_issue_links
    description: Base model for Gitlab.com issue links
    columns:
      - name: issue_link_id
        tests:
          - not_null
          - unique
      - name: issue_link_created_at
        tests:
          - not_null
      - name: issue_link_updated_at
        tests:
          - not_null

  - name: gitlab_dotcom_project_features
    description: Base model for Gitlab.com project features
    columns:
      - name: project_feature_id
        tests:
          - not_null
          - unique
      - name: project_id
        tests:
          - not_null
          - unique
      - name: project_features_created_at
        tests:
          - not_null
      - name: project_features_updated_at
        tests:
          - not_null

  - name: gitlab_dotcom_issue_metrics
    description: Base model for Gitlab.com issue metrics
    columns:
      - name: issue_metrics_id
        tests:
          - not_null
          - unique
      - name: issue_id
        tests:
          - not_null
      - name: issue_metrics_created_at
        tests:
          - not_null
      - name: issue_metrics_updated_at
        tests:
          - not_null
      - name: issue_metrics_updated_at
        description: last run of the script that imported this table

  - name: gitlab_dotcom_project_group_links
    description: Base model for Gitlab.com group links
    columns:
      - name: project_group_link_id
        tests:
          - not_null
          - unique
      - name: project_id
        tests:
          - not_null
          - relationships:
              to: ref('gitlab_dotcom_projects')
              field: project_id
      - name: group_id
        tests:
          - not_null
      - name: project_features_created_at
        tests:
          - not_null
      - name: project_features_updated_at
        tests:
          - not_null

  - name: gitlab_dotcom_issues
    description: Base model for Gitlab.com issues
    columns:
      - name: issue_id
        tests:
          - not_null
          - unique
      - name: issue_created_at
        tests:
          - not_null
      - name: issue_updated_at
        tests:
          - not_null
      - name: issue_title
        description: '{{ doc("visibility_documentation") }}'
      - name: issue_description
        description: '{{ doc("visibility_documentation") }}'
      - name: issues_last_updated_at
        description: last run of the script that imported this table


  - name: gitlab_dotcom_project_import_data
    description: Base model for Gitlab.com import data
    columns:
      - name: project_import_relation_id
        tests:
          - not_null
          - unique

  - name: gitlab_dotcom_label_links
    description: Base model for Gitlab.com label links
    columns:
      - name: label_link_id
        tests:
          - not_null
          - unique

  - name: gitlab_dotcom_project_mirror_data
    description: Base model for Gitlab.com mirror data
    columns:
      - name: project_mirror_data_id
        tests:
          - not_null
          - unique
      - name: project_id
        tests:
          - not_null

  - name: gitlab_dotcom_label_priorities
    description: Base model for Gitlab.com label priorities
    columns:
      - name: label_priority_id
        tests:
          - not_null
          - unique
      - name: project_id
        tests:
          - not_null
      - name: label_id
        tests:
          - not_null
      - name: label_priority_created_at
        tests:
          - not_null
      - name: label_priority_updated_at
        tests:
          - not_null

  - name: gitlab_dotcom_project_statistics
    description: Base model for Gitlab.com project statistics
    columns:
      - name: project_statistics_id
        tests:
          - not_null
          - unique
      - name: project_id
        tests:
          - not_null

  - name: gitlab_dotcom_labels
    description: Base model for Gitlab.com labels, the field `title` is hidden as contains PII.
    columns:
      - name: label_id
        tests:
          - not_null
          - unique

  - name: gitlab_dotcom_projects
    description: Base model for Gitlab.com projects
    columns:
      - name: project_id
        tests:
          - not_null
          - unique
      - name: project_created_at
        tests:
          - not_null
      - name: project_updated_at
        tests:
          - not_null
      - name: has_avatar
        description: This field will try to find an avatar file, if none is available will set this value as False, otherwise True.
      - name: visbility_level
        description: '{{ doc("visibility_documentation") }}'
      - name: project_description
        description: '{{ doc("visibility_documentation") }}'
      - name: project_import_source
        description: '{{ doc("visibility_documentation") }}'
      - name: project_issues_template
        description: '{{ doc("visibility_documentation") }}'
      - name: project_build_coverage_regex
        description: '{{ doc("visibility_documentation") }}'
      - name: project_name
        description: '{{ doc("visibility_documentation") }}'
      - name: project_path
        description: '{{ doc("visibility_documentation") }}'
      - name: project_import_url
        description: '{{ doc("visibility_documentation") }}'
      - name: project_merge_requests_template
        description: '{{ doc("visibility_documentation") }}'
      - name: project_description
        description: '{{ doc("visibility_documentation") }}'
      - name: projects_last_updated_at
        description: Last run of the script that loaded this table

  - name: gitlab_dotcom_licenses
    description: Base model for Gitlab.com licenses
    columns:
      - name: license_id
        tests:
          - not_null
          - unique
      - name: license_created_at
        tests:
          - not_null
      - name: license_updated_at
        tests:
          - not_null

  - name: gitlab_dotcom_subscriptions
    description: Base model for Gitlab.com subscriptions
    columns:
      - name: subscription_id
        tests:
          - not_null
          - unique
      - name: subscription_created_at
        tests:
          - not_null
          - unique
      - name: subscription_updated_at
        tests:
          - not_null
          - unique

  - name: gitlab_dotcom_members
    description: Base model for Gitlab.com members
    columns:
      - name: member_id
        tests:
          - not_null
          - unique
      - name: members_last_updated_at
        description: last run of the script that imported this table

  - name: gitlab_dotcom_users
    description: Base model for Gitlab.com users, the follwing fields are hidden for privacy `current_sign_in_ip`, `last_sign_in_ip`, `unconfirmed_email`, `website_url`, `notification_email`, `public_email`, `note`, `organization`
    columns:
      - name: user_id
        tests:
          - not_null
          - unique
      - name: user_created_at
        tests:
          - not_null
      - name: user_updated_at
        tests:
          - not_null
      - name: users_last_updated_at
        description: Last run of the script that loaded this table.

  - name: gitlab_dotcom_merge_request_diffs
    description: Base model for Gitlab.com request diffs
    columns:
      - name: merge_request_diff_id
        tests:
          - not_null
          - unique
      - name: merge_request_id
        tests:
          - not_null
          - relationships:
              to: ref('gitlab_dotcom_merge_requests')
              field: merge_request_id
      - name: merge_request_diff_created_at
        tests:
          - not_null
      - name: merge_request_diff_updated_at
        tests:
          - not_null
      - name: merge_request_diffs_last_updated_at
        description: Last run of the script that imported this table